import Category from '../models/Category';
import ApiError from '../utils/ApiError';
import httpStatus from 'http-status';
import * as defaultRepository from '../default';
import { ICategory, IDeleteCategory, IImage, IReq } from '../interfaces';
import Post from '../models/Post';
import Image from '../models/Image';
import { GoogleDriveService } from '../services/GoogleDriveService';

const createNewCategory = async (req: IReq): Promise<ICategory> => {
     const categoryExist = await Category.exists({ name: req.body.name });
     if (categoryExist) throw new ApiError(httpStatus.FORBIDDEN, 'Category already exists');

     return await defaultRepository.newCreate(Category, req, 'author');
};

const updateCategory = async (req: IReq): Promise<ICategory> => {
     const { id, name } = req.body;
     const category: ICategory | null = await Category.findOne({ _id: id });

     if (!category) throw new ApiError(httpStatus.NOT_FOUND, 'Category not found');
     if (category.author.toString() !== req.user._id) throw new ApiError(httpStatus.FORBIDDEN, "You don't have a permission for this action");
     if (category.name === name) throw new ApiError(httpStatus.FORBIDDEN, 'Category already exists');

     /**  updating category */
     Object.assign(category, { name });

     return await category.save();
};

const deleteCategory = async (req: IReq): Promise<IDeleteCategory> => {
     const category: ICategory | null = await Category.findOne({ _id: req.params.id });

     if (!category) throw new ApiError(httpStatus.NOT_FOUND, 'Category not found');
     if (category.author.toString() !== req.user._id) throw new ApiError(httpStatus.FORBIDDEN, "You don't have a permission for this action");

     /**  DELETE SELECTED CATEGORY */
     const deletedCategory = await Category.deleteOne({ _id: req.params.id });

     let postIDs: string[] = [];

     /**  GET ID'S OF RELATED POSTS */
     await Post.find({ fromCategory: req.params.id }).then((posts) => {
          posts.forEach((singlePost) => {
               postIDs.push(singlePost._id.toString());
          });
     });

     /**  DELETE ALL IMAGES PER SINGLE POST */
     for (const postID of postIDs) {
          const relatedImages: IImage[] = await Image.find({ postID });

          /**  DELETE ALL RELATED IMAGE RECORDS FROM DATABASE */
          await Image.deleteMany({ postID });

          if (relatedImages.length) {
               for (const img of relatedImages) {
                    await GoogleDriveService.deleteFromGoogleDrive(img.imageID);
               }
          }
     }

     /** DELETE ALL POSTS RELATED WITH  SELECTED CATEGORY */
     await Post.deleteMany({ fromCategory: req.params.id });

     return deletedCategory;
};

export default {
     createNewCategory,
     updateCategory,
     deleteCategory,
};
