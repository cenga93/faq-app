import jwt from 'jsonwebtoken';
import Token from '../models/Token';
import { IToken } from '../interfaces';
import config from '../config/config';

const generateResetToken = async (email: string): Promise<IToken> => {
     const resetPasswordToken: string = jwt.sign({ email }, config.JWT_SECRET_KEY, { expiresIn: config.JWT_RESET_EXPIRATION });

     interface ITokenData {
          token: string;
          type: string;
          user_email: string;
     }

     const tokenData: ITokenData = {
          token: resetPasswordToken,
          type: 'RESET_TOKEN',
          user_email: email,
     };

     return await new Token(tokenData).save();
};

export default {
     generateResetToken,
};
