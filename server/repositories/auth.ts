import httpStatus from 'http-status';
import jwt from 'jsonwebtoken';
import { getOne, isExists } from '../default';
import User, { IUserModel } from '../models/User';
import Token from '../models/Token';
import ApiError from '../utils/ApiError';
import { sendResetPasswordMail } from '../services/mailer';
import tokenRepository from './token';
import { IReq, IToken } from '../interfaces';
import config from '../config/config';
import { GoogleReCaptcha } from '../services/GoogleReCaptchaAnalytics';

const login = async (req: IReq): Promise<{ user: IUserModel; token: string }> => {
     const { email, password } = req.body;

     const user: IUserModel = await getOne(User, { email }, { code: false });

     /** Check if user exists in database or if password isn't correct */
     if (!user || !(await user.comparePassword(password))) {
          throw new ApiError(httpStatus.BAD_REQUEST, 'Incorrect email or password');
     }

     /** Check if user verified */
     if (!user.verified) {
          throw new ApiError(httpStatus.UNAUTHORIZED, 'Email not verified. Check your email');
     }

     /** Payload for token */
     const payload = {
          _id: user._doc._id,
          email: user._doc.email,
          role: user._doc.role,
          password: user._doc.password,
     };

     const token = jwt.sign(payload, config.JWT_SECRET_KEY, { expiresIn: config.JWT_LOGIN_EXPIRATION });

     delete user._doc.password;
     delete user._doc.__v;

     /** Google reCaptcha analytics */
     if (process.env.NODE_ENV === 'production') {
          await GoogleReCaptcha.analytics(req);
     }

     return {
          user,
          token,
     };
};

const resetPassword = async (req: IReq): Promise<void> => {
     const { email } = req.body;

     const user: IUserModel = await getOne(User, { email });
     if (!user) throw new ApiError(httpStatus.NOT_FOUND, 'User not found');

     const resetToken: IToken = await tokenRepository.generateResetToken(email);

     /** Google reCaptcha analytics */
     if (process.env.NODE_ENV === 'production') {
          await GoogleReCaptcha.analytics(req);
     }

     await sendResetPasswordMail(email, resetToken.token);
};

const updatePassword = async (req: IReq): Promise<void> => {
     const token: any = req.query.token;
     const { password } = req.body;

     return jwt.verify(token, config.JWT_SECRET_KEY, async (err: any, data: any) => {
          if (err) {
               throw new ApiError(httpStatus.UNAUTHORIZED, err.message);
          } else {
               /** Check if token exists in database */
               const resetToken: IToken = await isExists(Token, { token: token, blacklist: false, active: true });
               if (!resetToken) throw new ApiError(httpStatus.NOT_FOUND, 'Invalid token');

               /** Check if user exists in database */
               const user: IUserModel = await getOne(User, { email: data.email });
               if (!user) throw new ApiError(httpStatus.NOT_FOUND, 'User not found');

               Object.assign(user, { password });

               /** Disable token */
               const tokenFilter = { $and: [{ type: 'RESET_TOKEN' }, { user_email: data.email }, { token }] };
               await Token.findOneAndUpdate(tokenFilter, { active: false });

               /** Google reCaptcha analytics */
               if (process.env.NODE_ENV === 'production') {
                    await GoogleReCaptcha.analytics(req);
               }

               /** Update user's password */
               await user.save();
          }
     });
};

export default {
     login,
     resetPassword,
     updatePassword,
};
