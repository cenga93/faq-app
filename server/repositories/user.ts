import httpStatus from 'http-status';
import ApiError from '../utils/ApiError';
import { getOne, isExists } from '../default';
import User, { IUserModel } from '../models/User';
import { sendWelcomeMail } from '../services/mailer';
import { IReq, IUser } from '../interfaces';
import { GoogleReCaptcha } from '../services/GoogleReCaptchaAnalytics';

const createUser = async (req: IReq): Promise<IUser> => {
     const { body } = req;

     /**  Check if user exists in database */
     const userExists: boolean = await isExists(User, { email: body.email });
     if (userExists) throw new ApiError(httpStatus.FORBIDDEN, 'User already exists');

     /**  Generate code for account verification */
     body.code = Math.round(Math.random() * (9999 - 1000) + 1000);

     /**  Save new user in database */
     const newUser: IUserModel = await new User(body).save();

     /**  Send welcome mail */
     await sendWelcomeMail(newUser);

     /** Google reCaptcha analytics */
     if (process.env.NODE_ENV === 'production') {
          await GoogleReCaptcha.analytics(req);
     }

     return await newUser.getPublicFields();
};

const verifyUser = async (req: IReq): Promise<IUser> => {
     const _id: string = req.params.verifyId;
     const currentUser: IUserModel = await getOne(User, { _id, code: req.body.code });

     /** Check if user exists in database */
     if (!currentUser) throw new ApiError(httpStatus.FORBIDDEN, 'User not found');
     if (currentUser.verified) throw new ApiError(httpStatus.FORBIDDEN, 'User is already verified');

     /** Verifying user */
     Object.assign(currentUser, { verified: true });

     /** Update user in database */
     await currentUser.save();

     /** Google reCaptcha analytics */
     if (process.env.NODE_ENV === 'production') {
          await GoogleReCaptcha.analytics(req);
     }

     return await currentUser.getPublicFields();
};

export default {
     createUser,
     verifyUser,
};
