import Post from '../models/Post';
import { IImage, IPost, IReq } from '../interfaces';
import Image from '../models/Image';
import { GoogleDriveService } from '../services/GoogleDriveService';
import ApiError from '../utils/ApiError';
import httpStatus from 'http-status';
import { Schema } from 'mongoose';

const deleteSinglePost = async (req: IReq): Promise<void> => {
     const { postID } = req.params;

     /**  Check if post exists in database */
     const post: IPost | null = await Post.findOne({ _id: postID });
     if (!post) throw new ApiError(httpStatus.NOT_FOUND, 'Post not found');

     /**  Deleting post */
     await Post.deleteOne({ _id: postID });

     /**  Finding all related images with deleted post */
     const relatedImages: IImage[] = await Image.find({ postID });

     /**  Deleting all found images from database */
     await Image.deleteMany({ postID });

     /**  Deleting all found images from google cloud */
     if (relatedImages.length) {
          for (const img of relatedImages) {
               await GoogleDriveService.deleteFromGoogleDrive(img.imageID);
          }
     }
};

const uploadNewImage = async (req: IReq): Promise<IImage> => {
     const postImagesCount: number = await Image.countDocuments({ postID: req.body.postID });
     if (postImagesCount >= 5) throw new ApiError(httpStatus.FORBIDDEN, 'Max images count has exceeded');

     const data = await GoogleDriveService.uploadToGoogleDrive(req.files[0]);

     const body: {
          imageID: string | null | undefined;
          imageURL: string;
          postID: Schema.Types.ObjectId;
     } = {
          imageID: data.id,
          imageURL: `https://drive.google.com/uc?export=view&id=${data.id}`,
          postID: req.body.postID,
     };

     return await new Image(body).save();
};

const deleteSingleImage = async (req: IReq): Promise<void> => {
     const { imageID } = req.params;

     /**  Check if image exists in database */
     const image: IImage | null = await Image.findOne({ imageID });
     if (!image) throw new ApiError(httpStatus.NOT_FOUND, `Image not found ID: ${imageID}`);

     /**  Delete image refs from database */
     await Image.deleteOne({ imageID });

     /**  Delete image from cloud */
     await GoogleDriveService.deleteFromGoogleDrive(imageID);
};

const updatePost = async (req: IReq): Promise<IPost> => {
     interface IPopulation {
          path: string;
          select: { firstname: number; lastname: number };
     }

     const population: IPopulation = { path: 'author', select: { firstname: 1, lastname: 1 } };

     const post: IPost | null = await Post.findOne({ _id: req.body.postID, author: req.user._id }).populate(population);
     if (!post) throw new ApiError(httpStatus.NOT_FOUND, 'Post not found');

     Object.assign(post, req.body);

     return await post.save();
};

export default {
     deleteSinglePost,
     uploadNewImage,
     deleteSingleImage,
     updatePost,
};
