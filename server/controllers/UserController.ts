import { catchAsync } from 'catch-async-express';
import { Response } from 'express';
import httpStatus from 'http-status';
import userRepository from '../repositories/user';
import { IReq, IUser } from '../interfaces';

export const register = catchAsync(async (req: IReq, res: Response): Promise<void> => {
     const newUser: IUser = await userRepository.createUser(req);

     res.status(httpStatus.OK).json(newUser);
});

export const verify = catchAsync(async (req: IReq, res: Response): Promise<void> => {
     const verifiedUser: IUser = await userRepository.verifyUser(req);

     res.status(httpStatus.OK).json(verifiedUser);
});
