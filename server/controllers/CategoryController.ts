import { catchAsync } from 'catch-async-express';
import { Response } from 'express';
import httpStatus from 'http-status';
import categoryRepository from '../repositories/category';
import Category from '../models/Category';
import { ICategory, IReq } from '../interfaces';

export const create = catchAsync(async (req: IReq, res: Response): Promise<void> => {
     const newCategory = await categoryRepository.createNewCategory(req);

     res.status(httpStatus.OK).json(newCategory);
});

export const update = catchAsync(async (req: IReq, res: Response): Promise<void> => {
     const updatedCategory = await categoryRepository.updateCategory(req);

     res.status(httpStatus.OK).json(updatedCategory);
});

export const getAll = catchAsync(async (req: IReq, res: Response): Promise<void> => {
     const allCategories: ICategory[] = await Category.find().populate({
          path: 'author',
          select: { firstname: 1, lastname: 1 },
     });

     res.status(httpStatus.OK).json(allCategories);
});

export const deleteOne = catchAsync(async (req: IReq, res: Response): Promise<void> => {
     const deletedCategory = await categoryRepository.deleteCategory(req);

     res.status(httpStatus.OK).json(deletedCategory);
});
