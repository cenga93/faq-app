import { catchAsync } from 'catch-async-express';
import { Response } from 'express';
import httpStatus from 'http-status';
import * as defaultRepository from '../default';
import Post from '../models/Post';
import Image from '../models/Image';
import { IImage, IPost, IReq } from '../interfaces';
import mongoose from 'mongoose';
import postRepository from '../repositories/post';

export const create = catchAsync(async (req: IReq, res: Response): Promise<void> => {
     const newPost: IPost = await defaultRepository.newCreate(Post, req, 'author');

     res.status(httpStatus.OK).json(newPost);
});

export const update = catchAsync(async (req: IReq, res: Response): Promise<void> => {
     const updatedPost: IPost = await postRepository.updatePost(req);

     res.status(httpStatus.OK).json(updatedPost);
});

export const getAll = catchAsync(async (req: IReq, res: Response): Promise<void> => {
     const query = { $eq: new mongoose.Types.ObjectId(req.params.categoryID) };

     const postsFilter = { fromCategory: query };

     const allPosts: IPost[] = await Post.find(postsFilter)
          .populate({ path: 'author', select: { firstname: 1, lastname: 1 } })
          .populate({ path: 'fromCategory', select: { name: 1 } });

     res.status(httpStatus.OK).json(allPosts);
});

export const deletePost = catchAsync(async (req: IReq, res: Response): Promise<void> => {
     await postRepository.deleteSinglePost(req);

     res.status(200).json({ message: 'success' });
});

export const upload = catchAsync(async (req: IReq, res: Response): Promise<void> => {
     const newImage: IImage = await postRepository.uploadNewImage(req);

     res.status(200).json(newImage);
});

export const getImages = catchAsync(async (req: IReq, res: Response): Promise<void> => {
     const query = { $eq: new mongoose.Types.ObjectId(req.params.postID) };

     const images: IImage[] = await Image.find({ postID: query });

     res.status(httpStatus.OK).json(images);
});

export const deleteImage = catchAsync(async (req: IReq, res: Response): Promise<void> => {
     await postRepository.deleteSingleImage(req);

     res.status(httpStatus.NO_CONTENT).json();
});
