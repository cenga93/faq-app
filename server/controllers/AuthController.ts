import { catchAsync } from 'catch-async-express';
import { Response } from 'express';
import httpStatus from 'http-status';
import authRepository from '../repositories/auth';
import User, { IUserModel } from '../models/User';
import { IReq, IUser } from '../interfaces';
import ApiError from '../utils/ApiError';
import { getOne } from '../default';

export const login = catchAsync(async (req: IReq, res: Response): Promise<void> => {
     const data: { user: IUserModel; token: string } = await authRepository.login(req);

     res.cookie('token', data.token, {
          httpOnly: true,
          secure: true,
     });

     res.status(httpStatus.OK).json({ ...data.user._doc });
});

export const logout = catchAsync(async (req: IReq, res: Response): Promise<void> => {
     res.clearCookie('token');
     res.end();
});

export const resetPassword = catchAsync(async (req: IReq, res: Response): Promise<void> => {
     await authRepository.resetPassword(req);

     res.status(httpStatus.OK).json({
          message: 'success',
     });
});

export const updatePassword = catchAsync(async (req: IReq, res: Response): Promise<void> => {
     await authRepository.updatePassword(req);

     res.status(httpStatus.OK).json({
          message: 'success',
     });
});

export const me = catchAsync(async (req: IReq | any, res: Response): Promise<void> => {
     const user: IUserModel = await getOne(User, { email: req.user.email, password: req.user.password });
     if (!user) throw new ApiError(httpStatus.FORBIDDEN, 'User not found');

     const publicUserData: IUser = await user.getPublicFields();

     res.status(httpStatus.OK).json(publicUserData);
});
