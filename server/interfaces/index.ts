import { Schema } from 'mongoose';
import { Request } from 'express';

export interface IUser {
     _doc?: any;
     _id?: any;
     firstname: string;
     lastname: string;
     email: string;
     password?: string;
     code?: boolean;
     role: string;
     verified: boolean;
     createdAt: Date;
     updatedAt: Date;
}

export interface IFilter {
     _id?: any;
     email?: string;
     token?: string;
     code?: boolean;
     blacklist?: boolean;
     active?: boolean;
     password?: string;
}

export interface ISelect {
     _id?: number;
     code?: boolean;
     name?: number;
}

export interface IError {
     message: string;
     statusCode: number;
     isOperational?: boolean;
     stack?: string;
}

export interface IToken {
     token: string;
     createdAt: Date;
     updatedAt: Date;
}

export interface IImage {
     _id: Schema.Types.ObjectId;
     imageID: string | null | undefined;
     imageURL: string | null | undefined;
     postID: Schema.Types.ObjectId;
}

export interface IPost {
     _id?: Schema.Types.ObjectId;
     title: string;
     html: string;
     author: Schema.Types.ObjectId;
     fromCategory: Schema.Types.ObjectId;
     save(): Promise<IPost>;
}

export interface ICategory {
     name: string;
     author: Schema.Types.ObjectId;
     save(): Promise<ICategory>;
}

export interface IDeleteCategory {
     acknowledged: boolean;
     deletedCount: number;
}

export interface IGoogleCloud {
     filename: string;
     originalname: string;
     mimeType: string;
     buffer: Buffer;
}

export interface IReq extends Request {
     postID: Schema.Types.ObjectId;
     user: IUser;
     files?: any;
     captchaToken?: string;
}
