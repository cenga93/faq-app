import mongoose from 'mongoose';
import { errorConverter, errorHandler } from './middleware/error';
import app from './app';

mongoose.set('strictQuery', false);

/** Starting the server */
app.listen(process.env.PORT, async (): Promise<void> => {
     const URL: string | undefined = process.env.DATABASE_URL;

     console.log(`
     --------------------------------------
        [CONSOLE]  port::${process.env.PORT}
     --------------------------------------
     `);

     if (URL) {
          await mongoose
               .connect(URL)
               .then(({ connections }) => {
                    console.log(`==> Connected to [${connections[0].name}] database`);
               })
               .catch((err) => {
                    console.log('Could not connect to the database. Exiting now...', err);
               });
     }

     console.log('==> Server is up');

     /** Error middleware */
     app.use(errorConverter);
     app.use(errorHandler);
});
