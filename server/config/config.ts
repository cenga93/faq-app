interface ENV {
     PORT: number | undefined;
     DATABASE_URL: string | undefined;
     MAIL_USER: string | undefined;
     MAIL_PASSWORD: string | undefined;
     JWT_SECRET_KEY: string | undefined;
     JWT_LOGIN_EXPIRATION: string | undefined;
     JWT_RESET_EXPIRATION: string | undefined;
     NODE_ENV: string | undefined;
}

interface Config {
     NODE_ENV: string;
     PORT: number;
     DATABASE_URL: string;
     MAIL_USER: string;
     MAIL_PASSWORD: string;
     JWT_SECRET_KEY: string;
     JWT_LOGIN_EXPIRATION: string;
     JWT_RESET_EXPIRATION: string;
}

const getConfig = (): ENV => {
     return {
          NODE_ENV: process.env.NODE_ENV,
          PORT: process.env.PORT ? Number(process.env.PORT) : undefined,
          DATABASE_URL: process.env.DATABASE_URL,
          MAIL_USER: process.env.MAIL_USER,
          MAIL_PASSWORD: process.env.MAIL_PASSWORD,
          JWT_SECRET_KEY: process.env.JWT_SECRET_KEY,
          JWT_LOGIN_EXPIRATION: process.env.JWT_LOGIN_EXPIRATION,
          JWT_RESET_EXPIRATION: process.env.JWT_RESET_EXPIRATION,
     };
};

const getSanitizedConfig = (config: ENV): Config => {
     for (const [key, value] of Object.entries(config)) {
          if (value === undefined) {
               throw new Error(`Missing key ${key} in config.env`);
          }
     }
     return config as Config;
};

const config = getConfig();

const sanitizedConfig = getSanitizedConfig(config);

export default sanitizedConfig;
