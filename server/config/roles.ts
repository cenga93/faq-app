import { Roles } from './enums';
import { Permissions } from './permissions';
export const rolePermissions = new Map();

/** -------- USER -------- */
rolePermissions.set(Roles.USER, [
     // Auth
     Permissions.auth.READ,
     Permissions.auth.CREATE,
     Permissions.auth.UPDATE,
     Permissions.auth.DELETE,

     //   category
     Permissions.category.READ,
     Permissions.category.CREATE,
     Permissions.category.UPDATE,
     Permissions.category.DELETE,

     // post
     Permissions.post.READ,
     Permissions.post.CREATE,
     Permissions.post.UPDATE,
     Permissions.post.DELETE,
]);

/** -------- ADMIN -------- */
rolePermissions.set(Roles.ADMIN, [
     // Auth
     Permissions.auth.READ,
     Permissions.auth.CREATE,
     Permissions.auth.UPDATE,
     Permissions.auth.DELETE,
]);
