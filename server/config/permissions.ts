export const Permissions = {
     auth: {
          CREATE: 'user:create',
          READ: 'user:read',
          UPDATE: 'user:update',
          DELETE: 'user:delete',
     },
     category: {
          CREATE: 'category:create',
          READ: 'category:read',
          UPDATE: 'category:update',
          DELETE: 'category:delete',
     },
     post: {
          CREATE: 'post:create',
          READ: 'post:read',
          UPDATE: 'post:update',
          DELETE: 'post:delete',
     },
};
