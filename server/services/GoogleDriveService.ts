import { google } from 'googleapis';
import path from 'path';
import { IGoogleCloud } from '../interfaces';
import stream from 'stream';

export class GoogleDriveService {
     static getAuth = () => {
          return new google.auth.GoogleAuth({
               keyFile: path.join(__dirname, 'googleCloudConfig.json'),
               scopes: [`${process.env.GOOGLE_CLOUD_API}`],
          });
     };

     static uploadToGoogleDrive = async (fileObject: IGoogleCloud) => {
          const bufferStream = new stream.PassThrough();
          bufferStream.end(fileObject.buffer);

          const { data } = await google.drive({ auth: GoogleDriveService.getAuth(), version: 'v3' }).files.create({
               media: { mimeType: fileObject.mimeType, body: bufferStream },
               requestBody: { name: fileObject.originalname, parents: [`${process.env.FOLDER_ID}`] },
               fields: 'id',
          });

          return data;
     };

     static deleteFromGoogleDrive = async (imageID: any) => {
          const driveService = google.drive({
               auth: GoogleDriveService.getAuth(),
               version: 'v3',
          });

          return driveService.files.delete({ fileId: imageID });
     };
}
