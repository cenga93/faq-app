import axios from 'axios';
import { IReq } from '../interfaces';

export class GoogleReCaptcha {
     static analytics = async (req: IReq): Promise<void> => {
          const ip = req.headers['x-forwarded-for'] || req.socket.remoteAddress || null;

          await axios({
               url: `https://www.google.com/recaptcha/api/siteverify?&response=${req.body.captchaToken}&remoteip=${ip}&secret=${process.env.GOOGLE_ANALYTICS_SECRET_KEY}`,
               method: 'POST',
          });
     };
}
