import jwt from 'jsonwebtoken';
import httpStatus from 'http-status';
import { NextFunction, Request, Response } from 'express';
import { rolePermissions } from '../config/roles';
import ApiError from '../utils/ApiError.js';
import config from '../config/config';

export default (...requiredPermissions: any[]) =>
     (req: Request | any, res: Response, next: NextFunction) => {
          req.user = jwt.verify(req.cookies.token, config.JWT_SECRET_KEY);

          if (requiredPermissions.length) {
               const { role }: any = req.user;
               const userPermissions = rolePermissions.get(role);

               const hasRequiredPermissions = requiredPermissions.every((requiredPermission: any) =>
                    userPermissions.includes(requiredPermission)
               );

               if (!hasRequiredPermissions) {
                    throw new ApiError(httpStatus.UNAUTHORIZED, "You don't have a permission for this action");
               }
          }

          next();
     };
