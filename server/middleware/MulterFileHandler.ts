import Multer from 'multer';
import path from 'path';

export default class MulterFileHandler {
     static getInstance = () => {
          return Multer({
               storage: Multer.diskStorage({
                    destination: (req, res, cb) => {
                         cb(null, `${path.join(__dirname, '../', 'uploads')}`);
                    },
                    filename: (req, file, cb) => {
                         cb(null, file.fieldname + '_' + Date.now() + '_' + file.originalname);
                    },
               }),
               limits: {
                    fileSize: 5 * 1024 * 1024, // 5MB
               },
          });
     };
}
