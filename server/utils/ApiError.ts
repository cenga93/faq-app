export default class ApiError extends Error {
     public statusCode: number;

     constructor(statusCode: number, message: string) {
          super();
          this.statusCode = statusCode;
          this.message = message;

          Error.captureStackTrace(this, this.constructor);
     }
}
