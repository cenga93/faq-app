import Joi from 'joi';
import { objectId } from '../core/custom.validations';

export const create = {
     body: Joi.object().keys({
          title: Joi.string().required(),
          html: Joi.string().required(),
          author: Joi.string().custom(objectId).required(),
          fromCategory: Joi.string().custom(objectId).required(),
     }),
};

export const getAll = {
     params: Joi.object().keys({
          categoryID: Joi.string().custom(objectId).required(),
     }),
};

export const getImages = {
     params: Joi.object().keys({
          postID: Joi.string().custom(objectId).required(),
     }),
};

export const deleteImage = {
     params: Joi.object().keys({
          imageID: Joi.string().required(),
     }),
};

export const deletePost = {
     params: Joi.object().keys({
          postID: Joi.string().custom(objectId).required(),
     }),
};

export const editPost = {
     body: Joi.object().keys({
          title: Joi.string().required(),
          html: Joi.string().required(),
          postID: Joi.string().custom(objectId).required(),
     }),
};
