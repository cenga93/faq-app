import Joi from 'joi';
import { password, objectId } from '../core/custom.validations';

export const register = {
     body: Joi.object().keys({
          firstname: Joi.string().required(),
          lastname: Joi.string().required(),
          email: Joi.string().email().required(),
          password: Joi.string().required().custom(password),
          captchaToken: Joi.string().required(),
     }),
};

export const verify = {
     params: Joi.object().keys({
          verifyId: Joi.string().custom(objectId).required(),
     }),
     body: Joi.object().keys({
          code: Joi.string().required().length(4),
     }),
};

export const resetPassword = {
     body: Joi.object().keys({
          email: Joi.string().email().required(),
     }),
};

export const updatePassword = {
     query: Joi.object().keys({
          token: Joi.string().required(),
     }),
     body: Joi.object().keys({
          password: Joi.string().required().custom(password),
     }),
};

export const login = {
     body: Joi.object().keys({
          email: Joi.string().email().required(),
          password: Joi.string().required().custom(password),
          captchaToken: Joi.string().required(),
     }),
};
