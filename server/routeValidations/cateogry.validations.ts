import Joi from 'joi';
import { objectId } from '../core/custom.validations';

export const create = {
     body: Joi.object().keys({
          name: Joi.string().required(),
          author: Joi.string().custom(objectId).required(),
     }),
};

export const update = {
     body: Joi.object().keys({
          name: Joi.string().required(),
          id: Joi.string().custom(objectId).required(),
     }),
};

export const deleteValidation = {
     params: Joi.object().keys({
          id: Joi.string().custom(objectId).required(),
     }),
};
