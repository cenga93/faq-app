import { Model, model, Schema } from 'mongoose';
import { IImage } from '../interfaces';

const ImageSchema = new Schema(
     {
          imageID: {
               type: String,
               required: true,
          },
          imageURL: {
               type: String,
               required: true,
          },
          postID: {
               type: Schema.Types.ObjectId,
               ref: 'Post',
               required: true,
          },
     },
     {
          timestamps: true,
     }
);

const Image: Model<IImage> = model<IImage>('Image', ImageSchema);

export default Image;
