import { Document, Model, model, Schema } from 'mongoose';
import bcrypt from 'bcrypt';
import { IUser } from '../interfaces';

export interface IUserModel extends IUser, Document {
     comparePassword(password: string): Promise<boolean>;
     getPublicFields(): Promise<IUser>;
}

const UserSchema = new Schema(
     {
          firstname: {
               type: String,
               required: true,
          },
          lastname: {
               type: String,
               required: true,
          },
          password: {
               type: String,
               required: true,
          },
          email: {
               type: String,
               required: true,
               unique: true,
          },
          verified: {
               type: Boolean,
               default: false,
          },
          code: {
               type: String,
          },
          role: {
               type: String,
               default: 'user',
          },
     },
     {
          timestamps: true,
     }
);

UserSchema.methods.comparePassword = async function (userPassword: string): Promise<boolean> {
     const user: IUser = this as IUserModel;

     return await bcrypt.compare(userPassword, <string>user.password).catch(() => false);
};

/**  Hashing password */
UserSchema.pre('save', async function (next): Promise<void> {
     let user = this;

     // Only hash the password if it has been modified or if is password is new
     if (!user.isModified('password')) return next();

     // generate the salt
     const salt: string = await bcrypt.genSalt(10);
     user.password = bcrypt.hashSync(user.password, salt);

     return next();
});

/** Get public fields */
UserSchema.methods.getPublicFields = async function (): Promise<IUser> {
     const { firstname, lastname, email, verified, _id, role, createdAt, updatedAt }: IUser = this as IUserModel;
     return { firstname, lastname, email, verified, _id, role, createdAt, updatedAt };
};

const User: Model<IUserModel> = model<IUserModel>('User', UserSchema);

export default User;
