import { Model, model, Schema } from 'mongoose';
import { IPost } from '../interfaces';

const PostSchema = new Schema(
     {
          title: {
               type: String,
               required: true,
          },
          html: {
               type: String,
               required: true,
          },
          author: {
               type: Schema.Types.ObjectId,
               ref: 'User',
               required: true,
          },
          fromCategory: {
               type: Schema.Types.ObjectId,
               ref: 'Category',
               required: true,
          },
     },
     {
          timestamps: true,
     }
);

const Post: Model<IPost> = model<IPost>('Post', PostSchema);

export default Post;
