import { Model, model, Schema } from 'mongoose';
import { ICategory } from '../interfaces';

const CategorySchema = new Schema(
     {
          name: {
               type: String,
               required: true,
          },
          author: {
               type: Schema.Types.ObjectId,
               ref: 'User',
               required: true,
          },
     },
     {
          timestamps: true,
     }
);

const Category: Model<ICategory> = model<ICategory>('Category', CategorySchema);

export default Category;
