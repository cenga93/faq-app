import { Model, model, Schema } from 'mongoose';
import { IToken } from '../interfaces';

const UserSchema = new Schema(
     {
          token: {
               type: String,
               required: true,
          },
          type: {
               type: String,
               required: true,
          },
          user_email: {
               type: String,
               required: true,
          },
          active: {
               type: Boolean,
               required: false,
               default: true,
          },
          blacklist: {
               type: Boolean,
               required: false,
               default: false,
          },
     },
     {
          timestamps: true,
     }
);

const Token: Model<IToken> = model<IToken>('Token', UserSchema);

export default Token;
