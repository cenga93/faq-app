import { Router } from 'express';
import validate from '../middleware/validation';
import * as postValidation from '../routeValidations/post.validations';
import { create, getAll, upload, getImages, deleteImage, deletePost, update } from '../controllers/PostController';
import auth from '../middleware/auth';
import { Permissions } from '../config/permissions';
import multer from 'multer';

export default () => {
     const router = Router();
     const multerUpload = multer();

     router.get('/:categoryID', auth(Permissions.category.READ), validate(postValidation.getAll), getAll);
     router.get('/images/:postID', auth(Permissions.category.READ), validate(postValidation.getImages), getImages);
     router.post('/upload', auth(Permissions.category.CREATE), multerUpload.any(), upload);
     router.post('/create', auth(Permissions.category.CREATE), validate(postValidation.create), create);
     router.delete('/delete/:imageID', auth(Permissions.category.DELETE), validate(postValidation.deleteImage), deleteImage);
     router.delete('/delete/post/:postID', auth(Permissions.category.DELETE), validate(postValidation.deletePost), deletePost);
     router.patch('/:postID', auth(Permissions.category.UPDATE), validate(postValidation.editPost), update);

     return router;
};
