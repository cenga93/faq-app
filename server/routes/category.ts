import { Router } from 'express';
import validate from '../middleware/validation';
import * as categoryValidation from '../routeValidations/cateogry.validations';
import { create, getAll, deleteOne, update } from '../controllers/CategoryController';
import auth from '../middleware/auth';
import { Permissions } from '../config/permissions';

export default () => {
     const router = Router();

     router.get('/', auth(Permissions.category.READ), getAll);
     router.post('/create', auth(Permissions.category.CREATE), validate(categoryValidation.create), create);
     router.patch('/update', auth(Permissions.category.UPDATE), validate(categoryValidation.update), update);
     router.delete('/:id', auth(Permissions.category.DELETE), validate(categoryValidation.deleteValidation), deleteOne);

     return router;
};
