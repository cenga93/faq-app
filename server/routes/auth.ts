import { Router } from 'express';
import validate from '../middleware/validation';
import * as authValidation from '../routeValidations/auth.validations';
import { resetPassword, updatePassword, login, me, logout } from '../controllers/AuthController';
import { register, verify } from '../controllers/UserController';
import auth from '../middleware/auth';
import { Permissions } from '../config/permissions';

export default () => {
     const router = Router();

     router.post('/register', validate(authValidation.register), register);
     router.post('/reset-password/', validate(authValidation.resetPassword), resetPassword);
     router.post('/update-password/', validate(authValidation.updatePassword), updatePassword);
     router.post('/login', validate(authValidation.login), login);
     router.post('/logout', logout);
     router.post('/verification/:verifyId', validate(authValidation.verify), verify);
     router.get('/me', auth(Permissions.auth.READ), me);

     return router;
};
