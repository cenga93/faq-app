import { Router } from 'express';
import auth from './auth';
import post from './post';
import category from './category';

export default (): Router => {
     const router: Router = Router();

     router.use('/auth', auth());
     router.use('/post', post());
     router.use('/category', category());

     return router;
};
