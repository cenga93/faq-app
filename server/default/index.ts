import { Model } from 'mongoose';
import { IFilter, ISelect } from '../interfaces';
import { IReq } from '../interfaces';

export const newCreate = async (Collection: Model<any>, req: IReq, populatedFields?: any): Promise<any | null> => {
     const { body } = req;

     return await new Collection(body)
          .save()
          .then((response: { populate: (arg0: string | string[] | undefined) => any }) => response.populate(populatedFields));
};

export const isExists = async (Collection: Model<any>, filter: IFilter): Promise<any | null> => {
     return Collection.exists(filter);
};

export const getOne = async (Collection: any, filter: IFilter, notAllowedFields?: ISelect): Promise<any> => {
     return await Collection.findOne(filter).select(notAllowedFields);
};
