import dotenv from 'dotenv';
dotenv.config({ path: '.env' });
import express, { Express, Request, Response, NextFunction } from 'express';
import mongoose from 'mongoose';
import compression from 'compression';
import mongoSanitize from 'express-mongo-sanitize';
import cors from 'cors';
import path from 'path';
import router from './routes';
import cookieParser from 'cookie-parser';
const app: Express = express();
const origin = process.env.NODE_ENV === 'production' ? 'https://dev-cenga-faq.herokuapp.com/' : 'http://localhost:8080';

/** Sanitizes user-supplied data to prevent MongoDB Operator Injection. */
app.use(mongoSanitize());

/**  Compression middleware */
app.use(compression());

app.use(
     cors({
          credentials: true,
          origin: origin,
     })
);

app.use(cookieParser());

app.use((req: Request, res: Response, next: NextFunction) => {
     res.header('Access-Control-Allow-Origin', origin);
     res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
     res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
     res.header('Access-Control-Allow-Credentials', 'true');

     return next();
});

app.use(express.static('uploads'));

/** Parse application/x-www-form-urlencoded */
app.use(express.urlencoded({ extended: true }));

/** Parse application/json */
app.use(express.json());

mongoose.Promise = global.Promise;

/** Router */
app.use('/api', router());

if (process.env.NODE_ENV === 'production') {
     app.use(express.static(path.join(__dirname, 'dist')));

     app.get('*', (req: Request, res: Response) => {
          res.sendFile(path.join(__dirname, 'dist', 'index.html'));
     });
}

app.all('/api/*', function (req: Request, res: Response, next: NextFunction) {
     next();
});

export default app;
