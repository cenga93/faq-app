#!/bin/bash

npm run build;

echo "=> Frontend build process completed";
cp -r dist/ ../server/build;

echo "";
echo "=> Moved { dist } folder to: server/build ";

cd ../server/build/ || exit

git add .
git commit -am "DEPLOYMENT"
git push heroku master

heroku open;

echo "=> DEPLOY COMPLETED";
