import { helpers, minLength } from '@vuelidate/validators';

export const rules = {
     password: {
          minLength: minLength(6),
          containsPasswordRequirement: helpers.withMessage(
               () => `The password requires an uppercase, lowercase, number and special character`,
               (value) => {
                    const containsUppercase = /[A-Z]/.test(value);
                    const containsNumber = /[0-9]/.test(value);

                    return containsUppercase && containsNumber;
               }
          ),
     },
};
