import store from '@/store';

export default async ({ v$, state, recaptchaRef }) => {
     const isFormCorrect = await v$.$validate();
     recaptchaRef.value.reset();

     if (isFormCorrect && state.captchaToken) {
          await store.dispatch('login', state);
          state.captchaToken = '';
     }
};
