import { email, helpers, minLength, required } from '@vuelidate/validators';

export const rules = {
     firstname: {
          required,
     },
     lastname: {
          required,
     },
     email: {
          required,
          email,
     },
     password: {
          minLength: minLength(6),
          containsPasswordRequirement: helpers.withMessage(
               () => `The password requires an uppercase, lowercase, number and special character`,
               (value) => {
                    const containsUppercase = /[A-Z]/.test(value);
                    const containsNumber = /[0-9]/.test(value);

                    return containsUppercase && containsNumber;
               }
          ),
     },
};
