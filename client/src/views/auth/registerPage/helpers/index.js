import { rules } from './validationRules';
import handleSubmit from './handleSubmit';

export { rules, handleSubmit };
