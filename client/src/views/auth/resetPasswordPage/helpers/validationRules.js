import { email, required } from '@vuelidate/validators';

export const rules = {
     email: {
          required,
          email,
     },
};
