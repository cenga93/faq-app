import { maxLength, minLength, numeric, required } from '@vuelidate/validators';

export const rules = {
     code: {
          required,
          numeric,
          minLength: minLength(4),
          maxLength: maxLength(4),
     },
};
