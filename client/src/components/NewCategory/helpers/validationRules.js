import { minLength, required } from '@vuelidate/validators';

export const rules = {
     newCategory: {
          required,
          minLength: minLength(1),
     },
};
