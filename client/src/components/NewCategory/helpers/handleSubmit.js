import store from '@/store';

/**
 *  ADD NEW CATEGORY
 *
 * @param newCategory- This should be the empty ref
 * @param v$ - This should be the vuelidate object
 */
export default async ({ newCategory, v$ }) => {
     const validate = await v$.value.$validate();

     newCategory.value = newCategory.value.toLowerCase();

     if (validate) {
          const response = await store.dispatch('newCategory', newCategory.value);
          v$.value.$reset();

          return response;
     }
};
