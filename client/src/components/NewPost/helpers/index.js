import handleSubmit from './handleSubmit';
import { rules } from './validationRules';

export { handleSubmit, rules };
