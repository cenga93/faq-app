/**
 *  HANDLE DELETE SINGLE CATEGORY
 *
 * @param store - This should be store reference
 * @param item - This is single category from loop
 */
export default ({ store, item }) => {
     if (confirm(`Are you sure you want to delete ${item.name} category?`)) {
          store.dispatch('deleteOneCategory', item._id);
     }
};
