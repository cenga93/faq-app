/**
 *  HANDLE CHANGE CATEGORY NAME
 *
 * @param selectedCategory - This should be "selectedCategory" value from category store
 * @param item - This is single category from loop
 * @param store - This should be store reference
 */
export default async ({ selectedCategory, item, store }) => {
     if (selectedCategory._id === item._id) {
          return;
     }

     await store.dispatch('handleSelectedCategory', {
          _id: item._id,
          name: item.name,
          author: item.author,
     });

     await store.dispatch('handleChangeCategory');
};
