import store from '@/store';

/**
 * ADD NEW POST
 *
 * @param v$ - This should be the vuelidate object
 * @param title - This should be the empty ref
 * @param rte - This should be the ref of rte element
 */
export default async ({ v$, title, rteHTML }) => {
     const validate = await v$.value.$validate();

     if (validate) {
          await store.dispatch('editPost', {
               title: title.value,
               html: rteHTML.value,
          });

          v$.value.$reset();
     }
};
