import { minLength, required } from '@vuelidate/validators';

export const rules = {
     title: {
          required,
          minLength: minLength(1),
     },
};
