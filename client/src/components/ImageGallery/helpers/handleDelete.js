import store from '@/store';

/**
 *   DELETE SINGLE IMAGE FROM DATABASE AND FROM GOOGLE CLOUD
 *
 * @param imageID -This should be Google cloud id of image
 */
export default async ({ imageID }) => {
     if (confirm(`Are you sure you want to delete this image?`)) {
          await store.dispatch('deleteImage', imageID);
     }
};
