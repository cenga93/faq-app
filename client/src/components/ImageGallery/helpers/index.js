import handleUpload from './handleUpload';
import handleDelete from './handleDelete';

export { handleUpload, handleDelete };
