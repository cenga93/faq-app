import store from '@/store';

/**
 * GET IMAGE ON CHANGE INPUT
 *
 * @param e - This should be the event of input type file
 * @param route - This should be the  global route object
 * @param image - This should be the  base64 image object from input type file
 *
 *   1MB -> 1048576
 */
export default async ({ e, route, fileRef }) => {
     const file = e.target.files[0];

     if (!file) {
          return;
     }

     if (file.size > 2097152) {
          alert('Maximum allowed upload size is 2MB');
          return;
     }

     await store.dispatch('uploadImage', {
          image: file,
          postID: route.params.id,
     });

     if (fileRef.value) {
          fileRef.value.value = '';
     }
};
