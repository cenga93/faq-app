import { minLength, required } from '@vuelidate/validators';

export const rules = {
     currentCategory: {
          required,
          minLength: minLength(1),
     },
};
