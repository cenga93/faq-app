import { computed } from 'vue';
import store from '@/store';

/**
 * HANDLE NAV MENU
 *
 * @param menuRef - This should be reference of the menu element
 */

export default ({ menuRef }) => {
     window.addEventListener('click', async (e) => {
          if (!menuRef.value || !store.getters.getMenuActiveStatus) {
               return;
          }

          const outsideClick = !menuRef.value.contains(e.target) && !e.target.className.includes('avatar');
          const menuIsActive = computed(() => store.getters.getMenuActiveStatus);

          if (outsideClick && menuIsActive.value) {
               await store.dispatch('handleMenu', false);
          }
     });
};
