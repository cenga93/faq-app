/**
 * HANDLE SCROLL CATEGORY LIST
 *
 * @param element - This should be the reference of the element to scroll
 */
export default ({ element }) => {
     setTimeout(() => {
          element.scroll({
               top: element.scrollHeight,
               behavior: 'smooth',
          });
     }, 0);
};
