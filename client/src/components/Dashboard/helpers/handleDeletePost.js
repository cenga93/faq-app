/**
 *  HANDLE DELETE SINGLE POST
 *
 * @param store - This should be store reference
 * @param selectedPost -  This should be selectedCategory value from category store
 */
export default ({ store, selectedPost }) => {
     if (confirm(`Are you sure you want to delete this post?`)) {
          store.dispatch('deletePost', selectedPost);
     }
};
