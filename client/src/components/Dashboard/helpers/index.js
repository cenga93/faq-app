import handleDeletePost from './handleDeletePost';
import handleScroll from './handleScroll';

export { handleDeletePost, handleScroll };
