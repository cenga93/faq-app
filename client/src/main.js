import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import { QuillEditor } from '@vueup/vue-quill';
import '@vueup/vue-quill/dist/vue-quill.snow.css';

const userIsAuthenticated = localStorage.getItem('userIsAuthenticated') === 'true';

const app = createApp(App);

store.dispatch('setUser', userIsAuthenticated).then(() => {
     app.use(store).use(router).mount('#app');

     app.component('QuillEditor', QuillEditor);

     if (localStorage.getItem('data-mode')) {
          document.documentElement.setAttribute('data-mode', localStorage.getItem('data-mode'));
     }
});
