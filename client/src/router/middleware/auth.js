import { computed, nextTick } from 'vue';
import store from '@/store';
import router from '@/router';

export default async ({ meta }, from, next) => {
     const serverError = computed(() => store.getters.getServerError);

     await nextTick(() => {
          if (meta.title) {
               document.title = meta.title || 'FAQ';
          }
     });

     if (serverError.value) {
          await store.dispatch('setError', '');
     }

     if (meta.auth) {
          if (!store.getters.getUser) {
               await router.push({ name: 'LoginView' });
          } else {
               next();
          }
     } else {
          if (store.getters.getUser) {
               await router.push({ name: 'DashboardView' });
          }

          next();
     }
};
