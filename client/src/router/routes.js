import DashboardView from '@/views/DashboardView';
import router from '@/router/index';

export const routes = [
     {
          path: '/dashboard',
          name: 'DashboardView',
          component: DashboardView,
          meta: {
               auth: true,
          },
          children: [
               {
                    path: '',
                    name: 'PostList',
                    component: () => import('../views/PostList'),
                    meta: {
                         title: 'FAQ:: Dashboard',
                         auth: true,
                    },
               },
               {
                    path: '/post/:id',
                    name: 'PostDetails',
                    component: () => import('../views/Details'),
                    meta: {
                         title: 'FAQ:: Post details',
                         auth: true,
                    },
               },
          ],
     },
     {
          path: '/login',
          name: 'LoginView',
          component: () => import('../views/auth/loginPage'),
          meta: {
               title: 'FAQ:: Login',
               auth: false,
          },
     },
     {
          path: '/register',
          name: 'RegisterView',
          component: () => import('../views/auth/registerPage'),
          meta: {
               title: 'FAQ:: Register',
               auth: false,
          },
     },
     {
          path: '/verification/:id',
          name: 'VerifyView',
          component: () => import('../views/auth/accountVerificationPage'),
          meta: {
               title: 'FAQ:: Verification',
               auth: false,
          },
     },
     {
          path: '/reset-password',
          name: 'ResetPasswordView',
          component: () => import('../views/auth/resetPasswordPage'),
          meta: {
               title: 'FAQ:: Reset password',
               auth: false,
          },
     },
     {
          path: '/update-password',
          name: 'UpdatePasswordView',
          component: () => import('../views/auth/updatePasswordPage'),
          meta: {
               title: 'FAQ:: Update password',
               auth: false,
          },
          beforeEnter: (to, from, next) => {
               if (to.query && to.query.token) {
                    next();
               } else {
                    router.push({ name: 'LoginView' }).then(() => {});
               }
          },
     },
     {
          path: '/:pathMatch(.*)*',
          redirect: {
               name: 'DashboardView',
          },
     },
];
