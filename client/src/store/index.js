import { createStore } from 'vuex';
import auth from './modules/auth';
import modal from './modules/modal';
import post from './modules/post';
import category from './modules/category';
import spinner from './modules/spinner';
import menu from './modules/menu';
import error from './modules/error';

const store = createStore({
     modules: { auth, modal, category, post, spinner, menu, error },
});

export default store;
