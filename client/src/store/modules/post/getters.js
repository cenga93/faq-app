const getAllPosts = (state) => {
     return state.posts;
};

const getAllImages = (state) => {
     return state.images;
};

const getSelectedPost = (state) => {
     return state.selectedPost;
};

export default {
     getAllPosts,
     getAllImages,
     getSelectedPost,
};
