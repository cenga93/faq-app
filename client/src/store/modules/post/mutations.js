import MT from './types';

const setPosts = (state, posts) => {
     state.posts = posts;
};

const setImages = (state, images) => {
     state.images = images;
};

const setSinglePost = (state, postID) => {
     state.selectedPost = postID;
};

export default {
     [MT.SET_POSTS]: setPosts,
     [MT.SET_IMAGES]: setImages,
     [MT.SET_SINGLE_POST]: setSinglePost,
};
