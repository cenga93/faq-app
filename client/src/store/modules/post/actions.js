import axios from '@/axios';
import MT from './types';
import store from '@/store';
import router from '@/router';

const newPost = async ({ commit, dispatch }, payload) => {
     try {
          dispatch('handleSpinner', true);
          dispatch('setError', '');
          payload.author = store.getters.getUser._id;
          payload.fromCategory = store.getters.getSelectedCategory._id;

          const response = await axios.post('/post/create', payload, { withCredentials: true });
          const data = [...store.getters.getAllPosts, response.data];

          commit(MT.SET_POSTS, data);
          dispatch('handleModal', { active: false });
          dispatch('handleSpinner', false);
     } catch ({ message }) {
          dispatch('handleSpinner', false);
          dispatch('setError', message);
     }
};

const editPost = async ({ state, commit, dispatch }, payload) => {
     try {
          dispatch('handleSpinner', true);
          dispatch('setError', '');

          payload.postID = state.selectedPost._id;

          const response = await axios.patch(`/post/${payload.postID}`, payload, { withCredentials: true });

          state.posts = state.posts.map((post) => {
               if (post._id === payload.postID) {
                    return { ...response.data };
               }

               return post;
          });

          dispatch('handleSelectPost', { ...response.data });
          commit(MT.SET_POSTS, state.posts);
          dispatch('handleModal', { active: false });
          dispatch('handleSpinner', false);
     } catch ({ message }) {
          dispatch('handleSpinner', false);
          dispatch('setError', message);
     }
};

const getAllPosts = async ({ commit, dispatch }, payload) => {
     try {
          commit(MT.SET_POSTS, []);
          dispatch('setError', '');

          if (payload) {
               dispatch('handleSpinner', true);

               const response = await axios.get(`/post/${payload.categoryID}`, { withCredentials: true });

               commit(MT.SET_POSTS, response.data);
               dispatch('handleSpinner', false);
          }
     } catch ({ message }) {
          commit(MT.SET_POSTS, []);
          dispatch('handleSpinner', false);
          dispatch('setError', message);
     }
};

const handleSelectPost = async ({ commit, dispatch }, post) => {
     commit(MT.SET_SINGLE_POST, post);
     dispatch('getAllPostImages', post._id);
};

const getAllPostImages = async ({ commit, dispatch }, postID) => {
     try {
          dispatch('handleSpinner', true);
          dispatch('setError', '');

          const response = await axios.get(`/post/images/${postID}`, { withCredentials: true });

          commit(MT.SET_IMAGES, response.data);
          dispatch('handleSpinner', false);
     } catch ({ message }) {
          commit(MT.SET_IMAGES, []);
          dispatch('handleSpinner', false);
          dispatch('setError', message);
     }
};

const uploadImage = async ({ state, commit, dispatch }, payload) => {
     try {
          dispatch('handleSpinner', true);
          dispatch('setError', '');

          let formData = new FormData();
          formData.append('file', payload.image);
          formData.append('postID', payload.postID);

          const newImage = await axios.post('/post/upload', formData, { headers: { withCredentials: true } });

          const { imageID, imageURL, postID } = newImage.data;

          commit(MT.SET_IMAGES, [...state.images, { imageID, imageURL, postID }]);

          dispatch('handleSpinner', false);
     } catch ({ message }) {
          dispatch('handleSpinner', false);
          dispatch('setError', message);
     }
};

const deleteImage = async ({ state, commit, dispatch }, imageID) => {
     try {
          dispatch('handleSpinner', true);
          dispatch('setError', '');

          await axios.delete(`/post/delete/${imageID}`, { headers: { withCredentials: true } });

          const updatedImagesArray = state.images.filter((image) => image.imageID !== imageID);

          commit(MT.SET_IMAGES, updatedImagesArray);
     } catch ({ message }) {
          dispatch('setError', message);
     }

     dispatch('handleSpinner', false);
     dispatch('handleModal', { active: false });
};

const deletePost = async ({ state, commit, dispatch }, selectedPost) => {
     try {
          dispatch('handleSpinner', true);
          dispatch('setError', '');

          await axios.delete(`/post/delete/post/${selectedPost._id}`, { headers: { withCredentials: true } });

          const updatedPostArr = state.posts.filter((post) => post._id !== selectedPost._id);

          commit(MT.SET_POSTS, updatedPostArr);

          await router.push({ path: '/dashboard' });

          dispatch('handleModal', { active: false });
          dispatch('handleSpinner', false);
     } catch ({ message }) {
          dispatch('handleSpinner', false);
          dispatch('setError', message);
     }
};

export default {
     newPost,
     editPost,
     uploadImage,
     getAllPosts,
     getAllPostImages,
     handleSelectPost,
     deleteImage,
     deletePost,
};
