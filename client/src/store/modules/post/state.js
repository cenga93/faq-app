const createState = () => ({
     selectedPost: '',
     posts: [],
     images: [],
});

export default { ...createState() };
