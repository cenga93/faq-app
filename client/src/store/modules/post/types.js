export default {
     SET_POSTS: 'SET_POSTS',
     SET_IMAGES: 'SET_IMAGES',
     SET_SINGLE_POST: 'SET_SINGLE_POST',
};
