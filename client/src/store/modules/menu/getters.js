const getMenuActiveStatus = (state) => {
     return state.menuActive;
};

export default {
     getMenuActiveStatus,
};
