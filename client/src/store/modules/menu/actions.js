import MT from './types';

const handleMenu = async ({ commit }, payload) => {
     commit(MT.HANDLE_MENU, payload);
};

export default {
     handleMenu,
};
