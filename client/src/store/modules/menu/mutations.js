import MT from './types';

const handleMenu = (state, payload) => {
     state.menuActive = payload;
};

export default {
     [MT.HANDLE_MENU]: handleMenu,
};
