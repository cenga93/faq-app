import MT from './types';

const setCategories = (state, categories) => {
     state.categories = categories;
};

const updateCategories = (state, newCategory) => {
     state.categories = [...state.categories, newCategory];
};

const setSelectedCategory = (state, payload) => {
     state.selectedCategory = payload;
};

export default {
     [MT.SET_CATEGORIES]: setCategories,
     [MT.UPDATE_CATEGORIES]: updateCategories,
     [MT.SELECTED_CATEGORY]: setSelectedCategory,
};
