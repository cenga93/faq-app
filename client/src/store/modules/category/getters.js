const getAllCategories = (state) => {
     return state.categories;
};

const getSelectedCategory = (state) => {
     return state.selectedCategory;
};

export default {
     getAllCategories,
     getSelectedCategory,
};
