const createState = () => ({
     categories: [],
     selectedCategory: null,
});

export default { ...createState() };
