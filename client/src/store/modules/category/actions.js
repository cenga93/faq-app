import axios from '@/axios';
import store from '@/store';
import MT from './types';

const newCategory = async ({ commit, dispatch }, payload) => {
     try {
          dispatch('handleSpinner', true);
          dispatch('setError', '');

          const user = store.getters.getUser;
          const { data } = await axios.post('/category/create', { name: payload, author: user._id }, { withCredentials: true });

          const { _id, name, author } = data;
          const newSelectedCategory = { _id, name, author };

          commit(MT.UPDATE_CATEGORIES, data);

          dispatch('handleSelectedCategory', newSelectedCategory);
          dispatch('getAllPosts', { categoryID: data._id });
          dispatch('handleModal', { active: false });
          dispatch('handleSpinner', false);

          return true;
     } catch ({ message }) {
          dispatch('setError', message);
          dispatch('handleSpinner', false);

          return false;
     }
};

const updateCategory = async ({ dispatch, state }, payload) => {
     try {
          dispatch('handleSpinner', true);
          dispatch('setError', '');

          await axios.patch('/category/update', { name: payload.name, id: payload._id }, { withCredentials: true });

          state.categories = state.categories.map((category) => {
               if (category._id === payload._id) {
                    return { ...category, name: payload.name };
               }

               return category;
          });

          dispatch('handleModal', { active: false });
          dispatch('handleSpinner', false);
     } catch ({ message }) {
          dispatch('setError', message);
          dispatch('handleSpinner', false);
     }
};

const getAllCategories = async ({ commit, dispatch }) => {
     try {
          dispatch('handleSpinner', true);
          dispatch('setError', '');

          const { data } = await axios.get('/category', { withCredentials: true });

          commit(MT.SET_CATEGORIES, data);

          if (data.length) {
               const { _id, author, name } = data[0];

               commit(MT.SELECTED_CATEGORY, { _id, author, name });
          } else {
               dispatch('getAllPosts', null);
               commit(MT.SELECTED_CATEGORY, null);
          }

          dispatch('handleSpinner', false);
     } catch ({ message }) {
          dispatch('handleSpinner', false);
          dispatch('setError', message);

          commit(MT.SET_CATEGORIES, []);
     }
};

const handleChangeCategory = async ({ dispatch, state }) => {
     dispatch('getAllPosts', { categoryID: state.selectedCategory._id });
};

const handleSelectedCategory = async ({ commit }, payload) => {
     commit(MT.SELECTED_CATEGORY, payload);
};

const deleteOneCategory = async ({ commit, dispatch, state }, categoryID) => {
     try {
          dispatch('handleSpinner', true);
          dispatch('setError', '');

          await axios.delete(`category/${categoryID}`, { withCredentials: true });

          await dispatch('getAllCategories');
          await dispatch('getAllPosts', { categoryID: state.selectedCategory._id });

          dispatch('handleSpinner', false);
     } catch ({ message }) {
          dispatch('setError', message);
          commit(MT.SET_CATEGORIES, []);
          dispatch('handleSpinner', false);
     }

     dispatch('handleModal', { active: false });
};

export default {
     newCategory,
     getAllCategories,
     handleSelectedCategory,
     handleChangeCategory,
     deleteOneCategory,
     updateCategory,
};
