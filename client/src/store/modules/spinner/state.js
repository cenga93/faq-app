const createState = () => ({
     activeSpinner: false,
});

export default { ...createState() };
