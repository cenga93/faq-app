import MT from './types';

const handleSpinner = (state, active) => {
     state.activeSpinner = active;
};

export default {
     [MT.HANDLE_SPINNER]: handleSpinner,
};
