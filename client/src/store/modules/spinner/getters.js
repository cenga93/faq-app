const getSpinnerActiveStatus = (state) => {
     return state.activeSpinner;
};

export default {
     getSpinnerActiveStatus,
};
