import MT from './types';

const handleSpinner = async ({ commit }, active) => {
     commit(MT.HANDLE_SPINNER, active);
};

export default {
     handleSpinner,
};
