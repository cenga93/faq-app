const getModalActiveStatus = (state) => {
     return state.modalActive;
};

const getModalType = (state) => {
     return state.modalType;
};

const getModalData = (state) => {
     return state.modalData;
};

export default {
     getModalType,
     getModalActiveStatus,
     getModalData,
};
