import MT from './types';

const handleModal = async ({ commit, dispatch }, payload) => {
     commit(MT.HANDLE_MODAL, payload);
     dispatch('setModalType', payload.type);

     if (!payload.active) {
          dispatch('setError', '');
     }
};

const setModalType = async ({ commit }, modalType) => {
     commit(MT.SET_MODAL_TYPE, modalType ? modalType : '');
};

export default {
     handleModal,
     setModalType,
};
