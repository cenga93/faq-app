const createState = () => ({
     modalActive: false,
     modalType: '',
     modalData: null,
});

export default { ...createState() };
