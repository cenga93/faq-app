import MT from './types';

const handleModal = (state, payload) => {
     state.modalActive = payload.active;

     if (payload.modalData) {
          state.modalData = payload.modalData;
     }
};

const setModalType = (state, modalType) => {
     state.modalType = modalType;
};

export default {
     [MT.HANDLE_MODAL]: handleModal,
     [MT.SET_MODAL_TYPE]: setModalType,
};
