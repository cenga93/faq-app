const getServerError = (state) => {
     return state.error;
};

export default {
     getServerError,
};
