import MT from './types';

const setError = async ({ commit }, error) => {
     commit(MT.SET_ERROR, error);
};

export default {
     setError,
};
