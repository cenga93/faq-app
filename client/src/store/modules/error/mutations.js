import MT from './types';

const setError = (state, error) => {
     state.error = error;
};

export default {
     [MT.SET_ERROR]: setError,
};
