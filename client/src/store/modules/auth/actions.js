import router from '@/router';
import axios from '@/axios';
import MT from './types';

const login = async ({ commit, dispatch }, payload) => {
     try {
          dispatch('handleSpinner', true);
          dispatch('setError', '');

          for (const key in payload) {
               if (key !== 'password') {
                    payload[key] = payload[key].trim();
               }
          }

          const response = await axios.post('/auth/login/', payload, { withCredentials: true });

          localStorage.setItem('userIsAuthenticated', 'true');

          commit(MT.SET_USER, response.data);

          await router.push({ name: 'DashboardView' });
          dispatch('handleSpinner', false);
     } catch ({ message }) {
          commit(MT.SET_USER, null);
          dispatch('setError', message);
          dispatch('handleSpinner', false);
     }
};

const register = async ({ dispatch }, payload) => {
     try {
          dispatch('handleSpinner', true);
          dispatch('setError', '');

          for (const key in payload) {
               if (key !== 'password') {
                    payload[key] = payload[key].trim();
               }
          }

          const { status, statusText, data } = await axios.post('/auth/register', payload);

          if (status === 200 && statusText === 'OK') {
               await router.push({ path: `/verification/${data._id}` });
          }

          dispatch('handleSpinner', false);
     } catch ({ message }) {
          dispatch('setError', message);
          dispatch('handleSpinner', false);
     }
};

const verifyUser = async ({ dispatch }, payload) => {
     try {
          dispatch('setError', '');
          const response = await axios.post(`/auth/verification/${payload.id}`, { code: payload.code });

          if (response.status === 200 && response.statusText === 'OK') {
               await router.push({ name: 'LoginView' });
          }
     } catch ({ message }) {
          dispatch('setError', message);
     }
};

const resetPassword = async ({ dispatch }, payload) => {
     try {
          dispatch('handleSpinner', true);
          dispatch('setError', '');

          payload.email = payload.email.trim();

          const response = await axios.post(`/auth/reset-password/`, { email: payload.email });

          if (response.status === 200 && response.statusText === 'OK') {
               await router.push({ name: 'LoginView' });
          }

          dispatch('handleSpinner', false);
     } catch ({ message }) {
          dispatch('setError', message);
          dispatch('handleSpinner', false);
     }
};

const updatePassword = async ({ dispatch }, payload) => {
     try {
          dispatch('handleSpinner', true);
          dispatch('setError', '');

          for (const key in payload) {
               if (key !== 'password') {
                    payload[key] = payload[key].trim();
               }
          }

          const response = await axios.post(`/auth/update-password?token=${payload.token}`, { password: payload.password });

          if (response.status === 200 && response.statusText === 'OK') {
               await router.push({ name: 'LoginView' });
          }

          dispatch('handleSpinner', false);
     } catch ({ message }) {
          dispatch('setError', message);
          dispatch('handleSpinner', false);
     }
};

const setUser = async ({ commit, dispatch }, data) => {
     if (data) {
          try {
               const response = await axios.get('/auth/me/', { withCredentials: true });

               commit(MT.SET_USER, response.data);
          } catch ({ message }) {
               commit(MT.SET_USER, null);
               dispatch('setError', message);

               localStorage.setItem('userIsAuthenticated', 'false');

               await router.push({ name: 'LoginView' });
          }
     }
};

const logout = async ({ commit, dispatch }) => {
     try {
          dispatch('handleSpinner', true);
          dispatch('setError', '');

          await axios.post('/auth/logout/', { withCredentials: true });
          commit(MT.SET_USER, null);
          await router.push({ name: 'LoginView' });

          dispatch('handleSpinner', false);
     } catch ({ message }) {
          dispatch('handleSpinner', false);
          dispatch('setError', message);
     }
};

export default {
     login,
     logout,
     register,
     setUser,
     verifyUser,
     resetPassword,
     updatePassword,
};
