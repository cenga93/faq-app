import Axios from 'axios';
import { URL } from '../config';

Axios.defaults.withCredentials = true;
Axios.defaults.baseURL = URL.BASE_URL_SERVER;

const axios = Axios.create({ withCredentials: true });

axios.interceptors.request.use(
     (RequestConfig) => RequestConfig,
     (error) => Promise.reject(error.response.data)
);

axios.interceptors.response.use(
     (Response) => Response,
     (error) => Promise.reject(error.response.data)
);

export default axios;
