const { defineConfig } = require('@vue/cli-service');

module.exports = defineConfig({
     transpileDependencies: true,
     lintOnSave: true,
     pluginOptions: {},
     css: {
          loaderOptions: {
               scss: {
                    additionalData: `
                              @import "@/assets/scss/_vars.scss";
                              @import "@/assets/scss/_images.scss";
                              @import "@/assets/scss/_mixins.scss";
                              @import "@/assets/scss/_extends.scss";
                    `,
               },
          },
     },
});
