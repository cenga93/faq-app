## Design

![ ](projectImages/design.png)

---
## Design link:
`https://www.figma.com/file/URpOdfd2RZCDJpMaQSAAhC/FAQ?t=1Lm9nh5QEJViGO2q-1`

## Technologies and services:

> #### Frontend
>
> - Vue.js (composition API).
> - Vuex
> - Vue-router
> - Axios
> - SCSS style syntax
> - BEM methodology
> - eslint
> - babel

---

> #### Backend
>
> - Node.js
> - Express.js
> - Typescript
> - Mongo database
> - Mongoose
> - restAPI
> - httpOnly cookies

---

> #### Services
>
> - Google API
> - Google reCaptcha
> - Nodemailer

---

## Routes

```shell
Auth routes:
```

![ ](projectImages/authRoutes.png)

```shell
Category routes:
```

![ ](projectImages/categoryRoutes.png)

```shell
Post routes:
```

![ ](projectImages/postRoutes.png)

---
